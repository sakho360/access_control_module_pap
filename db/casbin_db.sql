--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)

-- Started on 2019-07-11 18:55:11 KST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16403)
-- Name: casbin_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.casbin_rule (
    ptype character varying(255) NOT NULL,
    v0 character varying(255) DEFAULT NULL::character varying,
    v1 character varying(255) DEFAULT NULL::character varying,
    v2 character varying(255) DEFAULT NULL::character varying,
    v3 character varying(255) DEFAULT NULL::character varying,
    v4 character varying(255) DEFAULT NULL::character varying,
    v5 character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.casbin_rule OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 24989)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24991)
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_roles (
    user_id bigint NOT NULL,
    roles character varying(255)
);


ALTER TABLE public.user_roles OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 24994)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    password character varying(255),
    username character varying(255)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 2900 (class 0 OID 16403)
-- Dependencies: 196
-- Data for Name: casbin_rule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.casbin_rule (ptype, v0, v1, v2, v3, v4, v5) FROM stdin;
p	admin	/*	POST	\N	\N	\N
p	admin	/*	GET	\N	\N	\N
p	user	/testPolicy	GET	\N	\N	\N
p	user	/testPolicy3	GET	\N	\N	\N
p	user2	/testPolicy2	GET	\N	\N	\N
\.


--
-- TOC entry 2902 (class 0 OID 24991)
-- Dependencies: 198
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_roles (user_id, roles) FROM stdin;
1	ROLE_USER
2	ROLE_USER
3	ROLE_USER
3	ROLE_ADMIN
\.


--
-- TOC entry 2903 (class 0 OID 24994)
-- Dependencies: 199
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, password, username) FROM stdin;
1	$2a$10$npTRrQe9qjRgJVZFNbcKuO/gGeiGvaqomDbcORelPs1oWnkoYd81e	user
2	$2a$10$XAWxRroiPDcrSmINpDkD7eZ9FN8zaKwH5iLuIBGMbQX1jDjEgURwS	user2
3	$2a$10$6KGH8shTIYtwHwHnhvVjs.kVzUf.EOUN5CrD5SILoqkSdUKX8iijG	admin
\.


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 197
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 3, true);


--
-- TOC entry 2777 (class 2606 OID 25001)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2778 (class 2606 OID 25002)
-- Name: user_roles fkhfh9dx7w3ubf1co1vdev94g3f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT fkhfh9dx7w3ubf1co1vdev94g3f FOREIGN KEY (user_id) REFERENCES public.users(id);


-- Completed on 2019-07-11 18:55:11 KST

--
-- PostgreSQL database dump complete
--

