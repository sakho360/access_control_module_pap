# Build stage
# FROM maven:3-jdk-8-alpine AS build
# COPY src /usr/app/src
# COPY pom.xml /usr/app
# COPY ./policies/authz_model.conf /usr/app/policies/
# RUN mvn -f /usr/app/pom.xml clean install

# Package stage
FROM java:8-jdk-alpine
#COPY --from=build /usr/app/target/pep-0.0.1-SNAPSHOT.jar /usr/app/target/
COPY ./target/pap-0.0.1-SNAPSHOT.jar /usr/app/target/
COPY ./policies/authz_model.conf /usr/app/policies/

WORKDIR /usr/app
RUN sh -c 'touch ./target/pap-0.0.1-SNAPSHOT.jar'
ENTRYPOINT [ "java", "-jar", "./target/pap-0.0.1-SNAPSHOT.jar"]