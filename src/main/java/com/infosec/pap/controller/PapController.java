package com.infosec.pap.controller;

import org.casbin.jcasbin.main.Enforcer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PapController {

	@Autowired
	private Enforcer enforcer;

	@RequestMapping("/")
	public String index(HttpServletRequest request) {
		String path = request.getRequestURI();
		String method = request.getMethod();
		return String.format("OK, path = %s, method = %s", path, method);
	}

	// Create
	@PostMapping("/addPolicy")
	public Map<String, Object> addPolicy(@RequestBody Map<String, String> policyMap) {
		String sub = policyMap.get("sub");
		String obj = policyMap.get("obj");
		String act = policyMap.get("act");
		Map<String, Object> response = new HashMap<>();

		if (sub != null & obj != null && act != null) {
			enforcer.addPolicy(sub, obj, act);
			response.put("error", false);
			response.put("message", "User/role '" + sub + "' has been authorized for '" + act + "' the '" + obj + "' ");
			return response;
		}
		response.put("error", true);
		response.put("message", "Failed, please include all as JSON obj {sub, obj, act}");
		return response;
	}

	// Create
	@PostMapping("/registerRole")
	public Map<String, Object> addRole(@RequestBody Map<String, String> roleMap) {
		String sub = roleMap.get("sub");
		String role = roleMap.get("role");
		Map<String, Object> response = new HashMap<>();

		if (sub != null & role != null) {
			enforcer.addGroupingPolicy(sub, role);
			response.put("error", false);
			response.put("message", "User '" + sub + "' has been registered for role '" + role + "'");
			return response;
		}
		response.put("error", true);
		response.put("message", "Failed, please include all as JSON obj {sub, obj, act}");
		return response;
	}

	// Create
	@PostMapping("/unregisterRole")
	public Map<String, Object> unregisterRole(@RequestBody Map<String, String> roleMap) {
		String sub = roleMap.get("sub");
		String role = roleMap.get("role");
		Map<String, Object> response = new HashMap<>();

		if (sub != null & role != null) {
			enforcer.removeGroupingPolicy(sub, role);
			response.put("error", false);
			response.put("message", "User '" + sub + "' has been unregistered from role '" + role + "'");
			return response;
		}
		response.put("error", true);
		response.put("message", "Failed, please include all as JSON obj {sub, obj, act}");
		return response;
	}

	// Update
	@PostMapping("/editPolicy")
	public Map<String, Object> editPolicy(@RequestBody Map<String, Object> policyMap) {
		Map<String, Object> response = new HashMap<>();
		response.put("error", true);
		response.put("message", "Failed, please include all as JSON obj {old:{sub, obj, act}, new:{sub, obj, act}}");
		try {
			Map<String, String> oldPolicy = (Map<String, String>) policyMap.get("old");
			if (oldPolicy == null) {
				return response;
			}
			String sub = oldPolicy.get("sub");
			String obj = oldPolicy.get("obj");
			String act = oldPolicy.get("act");

			Map<String, String> newPolicy = (Map<String, String>) policyMap.get("new");
			if (newPolicy == null) {
				return response;
			}
			String newSub = newPolicy.get("sub");
			String newObj = newPolicy.get("obj");
			String newAct = newPolicy.get("act");

			if (newSub != null && sub != null & newObj != null && obj != null && newAct != null && act != null) {
				enforcer.removePolicy(sub, obj, act);
				enforcer.addPolicy(newSub, newObj, newAct);
				response.put("error", false);
				response.put("message", "Policy has been modified. " + "User/role '" + newSub
						+ "' has been authorized for '" + newAct + "' the '" + newObj + "' ");
			}
		} catch (ClassCastException e) {
			// return response;
		}

		return response;
	}

	// Read
	@GetMapping("/viewPolicy")
	public List<Map<String, String>> viewPolicy() {
		List<List<String>> policyList = enforcer.getPolicy();

		List<Map<String, String>> policyMapList = new ArrayList<>();

		for (List<String> pList : policyList) {
			Map<String, String> policyMap = new HashMap<>();
			policyMap.put("sub", pList.get(0));
			policyMap.put("obj", pList.get(1));
			policyMap.put("act", pList.get(2));
			policyMapList.add(policyMap);
		}
		return policyMapList;
	}

	// Read
	@GetMapping("/getPolicy")
	public List<Map<String, String>> getPolicyByUser(@RequestParam String sub) {
		List<List<String>> policyList = enforcer.getImplicitPermissionsForUser(sub);

		List<Map<String, String>> policyMapList = new ArrayList<>();

		for (List<String> pList : policyList) {
			Map<String, String> policyMap = new HashMap<>();
			policyMap.put("sub", pList.get(0));
			policyMap.put("obj", pList.get(1));
			policyMap.put("act", pList.get(2));
			policyMapList.add(policyMap);
		}
		return policyMapList;
	}

	// Read
	@GetMapping("/getRole")
	public List<String> getRole(@RequestParam String sub) {
		List<String> policyList = enforcer.getRolesForUser(sub);
		return policyList;
	}

	// Read
	@GetMapping("/viewRole")
	public List<Map<String, String>> viewRole() {
		List<List<String>> policyList = enforcer.getGroupingPolicy();

		List<Map<String, String>> policyMapList = new ArrayList<>();

		for (List<String> pList : policyList) {
			Map<String, String> policyMap = new HashMap<>();
			policyMap.put("sub", pList.get(0));
			policyMap.put("role", pList.get(1));
			policyMapList.add(policyMap);
		}
		return policyMapList;
	}

	// Delete
	@PostMapping("/deletePolicy")
	public Map<String, Object> deletePolicy(@RequestBody Map<String, String> policyMap) {
		String sub = policyMap.get("sub");
		String obj = policyMap.get("obj");
		String act = policyMap.get("act");

		Map<String, Object> response = new HashMap<>();

		if (sub != null & obj != null && act != null) {
			enforcer.removePolicy(sub, obj, act);
			response.put("error", false);
			response.put("message",
					"Policy of user/role '" + sub + "' for '" + act + "' the '" + obj + "' has been deleted");
			return response;
		}
		response.put("error", true);
		response.put("message", "Failed, please include all as JSON obj {sub, obj, act}");
		return response;
	}

}
