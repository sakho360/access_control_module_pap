package com.infosec.pap.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.infosec.pap.DataInitializer;

import org.casbin.jcasbin.main.Enforcer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

@Component
public class JwtTokenAuthenticationFilter extends GenericFilterBean {

    private Logger log = LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);
    private JwtTokenProvider jwtTokenProvider;    
    
    @Autowired
    private Enforcer enforcer;
    
    public JwtTokenAuthenticationFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }    
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
        throws IOException, ServletException {

       
        String spath = ((HttpServletRequest) req).getServletPath().toString();
        String method = ((HttpServletRequest) req).getMethod().toString();
        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
         //log.info("url : " + url);
        log.info("spath : " + spath);
        log.info("method : " + method);
      
        if(!spath.equals("/error")) {
            try {
                if (token != null && jwtTokenProvider.validateToken(token)) {
                    Authentication auth = jwtTokenProvider.getAuthentication(token);
        
                    String username = jwtTokenProvider.getUsername(token);
                    //String url = ((HttpServletRequest) req).getRequestURL().toString();
                    
        
                    //log.info("token : " + token);
                    log.info("username : " + username);
                    
        
                    enforcer = DataInitializer.ENFORCER;
                    if(enforcer != null) {
                    	boolean allow = false;
                    	if(method.equals("GET")) {
                    		allow = enforcer.enforce(username, "pap", "read");
                    	}
                    	else if(method.equals("POST") || method.equals("PUT")) {
                    		allow = enforcer.enforce(username, "pap", "write");
                    	}
                        
                        if(!allow) {
                            HttpServletResponse resp = (HttpServletResponse) res;
                            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not authorized to access this resource");
                            return;
                        }
                        log.info("allow : " + allow);
                    } else {
                        log.info("enforcer : null");
                    }
        
                    if (auth != null) {
                        SecurityContextHolder.getContext().setAuthentication(auth);
                    }
                }
            } catch (InvalidJwtAuthenticationException e) {
                HttpServletResponse resp = (HttpServletResponse) res;
                resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Expired or invalid JWT token");
                return;
            }
        }
        
        filterChain.doFilter(req, res);
    }

}